package demo.util;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ResourceAssemblerUtils {

    private ResourceAssemblerUtils() {
    }

    public static Link addSelfLink(ResourceSupport resourceSupport, String rel) {
        checkNotNull(resourceSupport, "resoursSupport can't be null");
        return resourceSupport.getLink("self").withRel(rel);
    }

}
