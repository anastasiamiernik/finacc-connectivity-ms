package demo.domain.eventdriven.event.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import demo.domain.eventdriven.event.BasicEventService;
import demo.domain.eventdriven.event.EventProperties;
import demo.domain.eventdriven.event.EventRepository;
import demo.domain.eventdriven.event.EventService;
import demo.domain.eventdriven.event.EventSource;

@Configuration
@ConditionalOnClass({EventRepository.class, Source.class, RestTemplate.class})
@ConditionalOnMissingBean(EventService.class)
@EnableConfigurationProperties(EventProperties.class)
public class EventAutoConfigurer {

    private EventRepository eventRepository;
    private RestTemplate restTemplate;

    public EventAutoConfigurer(EventRepository eventRepository, RestTemplate restTemplate) {
        this.eventRepository = eventRepository;
        this.restTemplate = restTemplate;
    }

    @SuppressWarnings("unchecked")
    @Bean
    public EventService eventService(EventSource eventSource) {
        return new BasicEventService(eventRepository, eventSource, restTemplate);
    }

    @Bean
    public EventSource eventSource(Source source) {
        return new EventSource(source.output());
    }
}
