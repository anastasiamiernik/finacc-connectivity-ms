package demo.domain.eventdriven.event;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.hateoas.Link;

import demo.domain.eventdriven.Aggregate;

/**
 * Service interface to manage {@link Event}s.
 */
public interface EventService<T extends Event, ID extends Serializable> {

    /**
     * Fires a synchronous domain event to be applied to an entity through HTTP requests/responses.
     *
     * @return the applied {@link Event}
     */
    <E extends Aggregate, S extends T> S send(S event, Link... links);

    /**
     * Fires an asynchronous domain event to be applied to an entity through AMQP messages.
     *
     * @return a flag indicating if the {@link Event} message was sent successfully
     */
    <S extends T> Boolean sendAsync(S event, Link... links);

    /**
     * Saves a given event entity.
     *
     * @return the saved event entity
     */
    <S extends T> S save(S event);

    /**
     * Saves a given event entity. The {@link ID} parameter is the unique {@link Event} identifier.
     *
     * @return the saved event entity
     */
    <S extends T> S save(ID id, S event);

    /**
     * Finds an {@link Event} entity by the event id.
     *
     * @return the {@link Event} entity with the given id or {@literal null} if none found
     */
    <EID extends ID> Optional<T> findOne(EID id);

    /**
     * Find entity's {@link Event}s by the entity's id.
     *
     * @return a collection of {@link Event}s
     */
    <E extends Events> E find(ID entityId);
}
