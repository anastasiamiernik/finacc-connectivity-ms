package demo.domain;

import java.io.Serializable;

import org.springframework.hateoas.Identifiable;

public interface IdentifiableEntity<ID extends Serializable> extends Identifiable<ID> {

}
