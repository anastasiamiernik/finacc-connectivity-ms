package demo.authdeposit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class AuthDepositNotFoundException extends RuntimeException {

    public AuthDepositNotFoundException() {
    }

    public AuthDepositNotFoundException(String message) {
        super(message);
    }

    public AuthDepositNotFoundException(Throwable cause) {
        super(cause);
    }

    public AuthDepositNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
