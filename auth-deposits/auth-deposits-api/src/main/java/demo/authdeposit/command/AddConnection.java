package demo.authdeposit.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositService;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositEvent;
import demo.connection.domain.ConnectionService;
import demo.domain.eventdriven.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static demo.authdeposit.event.AuthDepositVerificationEventType.AUTH_DEPOSIT_INITIATED;

@Service
@Transactional
public class AddConnection extends Command<AuthDeposit> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddConnection.class);
    private final ConnectionService connectionService;
    private final AuthDepositService authDepositService;

    public AddConnection(ConnectionService connectionService, AuthDepositService authDepositService) {
        this.connectionService = connectionService;
        this.authDepositService = authDepositService;
    }

    public AuthDeposit execute(AuthDeposit authDeposit, Long connectionId) {
        checkNotNull(authDeposit.getConnection());
        checkState(authDeposit.getConnection().getIdentity().equals(connectionId), "Connection with id "
                + connectionId + "already added");
        // Make assumptions on the aggregate's state
        checkState(authDeposit.getState() == AuthDepositVerificationState.CONNECTION_CREATED,
                "Auth Deposit Verification State should be CONNECTION CREATED");

        authDeposit.setConnection(connectionService.get(connectionId)
                .orElseThrow(() -> new RuntimeException("Connection with id " + connectionId + "not found")));
        authDeposit.setState(AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED);
        authDeposit = authDepositService.update(authDeposit);

        try {
            authDeposit.sendAsyncEvent(new AuthDepositEvent(AUTH_DEPOSIT_INITIATED, authDeposit));
        } catch (Exception exception) {
            LOGGER.error("Could not add connection to auth deposit", exception);
            authDeposit.setConnection(null);
            authDeposit.setState(AuthDepositVerificationState.CONNECTION_CREATED);
            // Rollback to previous state
            authDeposit = authDepositService.update(authDeposit);
        }

        return authDeposit;
    }
}
