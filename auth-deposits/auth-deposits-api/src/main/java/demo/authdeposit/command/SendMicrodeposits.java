package demo.authdeposit.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositService;
import demo.authdeposit.event.AuthDepositEvent;
import demo.domain.eventdriven.Command;
import demo.util.ResourceAssemblerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkState;
import static demo.authdeposit.domain.AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED;
import static demo.authdeposit.event.AuthDepositVerificationEventType.AUTH_DEPOSIT_SUBMITTED;
import static demo.authdeposit.event.AuthDepositVerificationEventType.AUTH_DEPOSIT_VERIFICATION_FAILED;

@Service
@Transactional
public class SendMicrodeposits extends Command<AuthDeposit> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendMicrodeposits.class);
    private final AuthDepositService authDepositService;

    public SendMicrodeposits(AuthDepositService authDepositService) {
        this.authDepositService = authDepositService;
    }

    public AuthDeposit execute(AuthDeposit authDeposit) {
        // Make assumptions on the aggregate's state
        checkState(authDeposit.getState() == AUTH_DEPOSIT_INITIATED,
                "Auth Deposit State should be " + AUTH_DEPOSIT_INITIATED);

        // FIXME: add impl via external services

        try {
            authDeposit.sendAsyncEvent(new AuthDepositEvent(AUTH_DEPOSIT_SUBMITTED, authDeposit));
        } catch (Exception exception) {
            LOGGER.error("Failed to send microdeposits", exception);
            // Trigger connection failed event
            AuthDepositEvent event = new AuthDepositEvent(AUTH_DEPOSIT_VERIFICATION_FAILED, authDeposit);
            event.add(ResourceAssemblerUtils.addSelfLink(authDeposit, "deposit"));
            authDeposit.sendAsyncEvent(event);
        }

        return authDeposit;
    }
}
