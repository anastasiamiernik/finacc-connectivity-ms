package demo.authdeposit.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositService;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositEvent;
import demo.domain.eventdriven.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkState;
import static demo.authdeposit.domain.AuthDepositVerificationState.AUTH_DEPOSIT_TRANSFERRED;
import static demo.authdeposit.event.AuthDepositVerificationEventType.AUTH_DEPOSIT_VERIFIED;

@Service
@Transactional
public class VerifyMicrodeposits extends Command<AuthDeposit> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerifyMicrodeposits.class);

    private final AuthDepositService authDepositService;

    public VerifyMicrodeposits(AuthDepositService authDepositService) {
        this.authDepositService = authDepositService;
    }

    public AuthDeposit execute(AuthDeposit authDeposit) {
        // Make assumptions on the aggregate's state
        checkState(authDeposit.getState() == AUTH_DEPOSIT_TRANSFERRED,
                "Auth Deposit State should be " + AUTH_DEPOSIT_TRANSFERRED);

        // FIXME: add impl via external services

        try {
            authDeposit.sendAsyncEvent(new AuthDepositEvent(AUTH_DEPOSIT_VERIFIED, authDeposit));
        } catch (Exception exception) {
            LOGGER.error("Failed to verify microdeposits", exception);
            // Rollback to previous state
            authDeposit.setState(AuthDepositVerificationState.AUTH_DEPOSIT_VERIFIED);
            authDeposit = authDepositService.update(authDeposit);
        }

        return authDeposit;
    }
}
