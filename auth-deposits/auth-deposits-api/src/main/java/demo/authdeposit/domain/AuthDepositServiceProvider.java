package demo.authdeposit.domain;

import org.springframework.stereotype.Service;

import demo.authdeposit.event.AuthDepositEvent;
import demo.connection.domain.ConnectionService;
import demo.domain.eventdriven.AggregateServiceProvider;
import demo.domain.eventdriven.event.EventService;

@Service
public class AuthDepositServiceProvider extends AggregateServiceProvider<AuthDeposit> {

    private final AuthDepositService authDepositService;
    private final ConnectionService connectionService;
    private final EventService<AuthDepositEvent, Long> eventService;

    public AuthDepositServiceProvider(AuthDepositService authDepositService, ConnectionService connectionService,
            EventService<AuthDepositEvent, Long> eventService) {
        this.authDepositService = authDepositService;
        this.connectionService = connectionService;
        this.eventService = eventService;
    }

    public AuthDepositService getAuthDepositService() {
        return authDepositService;
    }

    public ConnectionService getConnectionService() {
        return connectionService;
    }

    public EventService<AuthDepositEvent, Long> getEventService() {
        return eventService;
    }

    @Override
    public AuthDepositService getDefaultService() {
        return authDepositService;
    }

    @Override
    public EventService<AuthDepositEvent, Long> getDefaultEventService() {
        return eventService;
    }
}
