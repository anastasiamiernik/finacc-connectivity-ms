package demo.authdeposit.event;

import demo.domain.eventdriven.event.EventRepository;

public interface AuthDepositEventRepository extends EventRepository<AuthDepositEvent, Long> {

}
