package demo.authdeposit.event;

/**
 * The {@link AuthDepositVerificationEventType} represents events that describe the transitions of
 * {@link demo.authdeposit.domain.AuthDepositVerificationState} in the course of auth deposit verification workflow.
 */
public enum AuthDepositVerificationEventType {
    CONNECTION_INITIATED,
    CONNECTION_CREATED,
    CONNECTION_FAILED,
    AUTH_DEPOSIT_FAILED,
    AUTH_DEPOSIT_INITIATED,
    AUTH_DEPOSIT_REQUESTED,
    AUTH_DEPOSIT_SUBMITTED,
    AUTH_DEPOSIT_TRANSFERRED,
    AUTH_DEPOSIT_VERIFICATION_FAILED,
    AUTH_DEPOSIT_VERIFIED
}
