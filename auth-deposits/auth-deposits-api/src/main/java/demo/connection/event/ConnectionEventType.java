package demo.connection.event;

import demo.connection.domain.Connection;

/**
 * The {@link ConnectionEventType} represents events that describe state transitions of a {@link Connection} aggregate.
 */
public enum ConnectionEventType {
    CONNECTION_INITIATED,
    CONNECTION_CREATED,
    CONNECTION_FAILED
}
