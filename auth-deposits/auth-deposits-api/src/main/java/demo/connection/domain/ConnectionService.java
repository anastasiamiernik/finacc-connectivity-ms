package demo.connection.domain;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.hateoas.TemplateVariable;
import org.springframework.hateoas.UriTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.domain.eventdriven.CrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link CrudService} implementation for {@link Connection} domain object.
 */
@Service
public class ConnectionService extends CrudService<Connection, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionService.class);

    private final RestTemplate restTemplate;

    public ConnectionService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Optional<Connection> get(Long connectionId) {
        Connection connection;
        try {
            connection = restTemplate.getForObject(new UriTemplate("http://connections-api/v1/connections/{id}")
                    .with("id", TemplateVariable.VariableType.PATH_VARIABLE)
                    .expand(connectionId), Connection.class);
        } catch (RestClientResponseException exception) {
            LOGGER.error("Get connection failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return Optional.ofNullable(connection);
    }

    @Override
    public Connection create(Connection connection) {
        Connection result;
        try {
            result = restTemplate.postForObject(new UriTemplate("http://connections-api/v1/connections").expand(),
                    connection, Connection.class);
        } catch (RestClientResponseException exception) {
            LOGGER.error("Create connection failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return result;
    }

    @Override
    public Connection update(Connection entity) {
        Connection result = null;
        try {
            result = restTemplate.exchange(new RequestEntity<>(entity, HttpMethod.PUT,
                    new UriTemplate("http://connections-api/v1/connections/{id}")
                            .with("id", TemplateVariable.VariableType.PATH_VARIABLE)
                            .expand(entity.getIdentity())), Connection.class).getBody();
        } catch (RestClientResponseException exception) {
            LOGGER.error("Update connection failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return result;
    }

    @Override
    public boolean delete(Long connectionId) {
        try {
            restTemplate.delete(new UriTemplate("http://connections-api/v1/connections/{id}")
                    .with("id", TemplateVariable.VariableType.PATH_VARIABLE).expand(connectionId));
        } catch (RestClientResponseException exception) {
            LOGGER.error("Delete conection failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return true;
    }

    private String getHttpStatusMessage(RestClientResponseException exception) {
        Map<String, String> errorMap = new HashMap<>();
        try {
            errorMap = new ObjectMapper().readValue(exception.getResponseBodyAsString(), errorMap.getClass());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return errorMap.getOrDefault("message", null);
    }
}
