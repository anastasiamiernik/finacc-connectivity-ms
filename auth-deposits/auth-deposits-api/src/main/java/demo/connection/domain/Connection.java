package demo.connection.domain;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import demo.authdeposit.api.v1.AuthDepositController;
import demo.connection.event.ConnectionEvent;
import demo.domain.eventdriven.AbstractEventDrivenEntity;
import demo.domain.eventdriven.Aggregate;
import demo.domain.eventdriven.AggregateServiceProvider;
import lombok.Builder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * A connection is a representation of the link between end user and a specific financial institution.
 * It’s through this connection that an interface with an institution is established
 * and the user’s account data is synced.
 */
@Builder
@Entity
public class Connection extends AbstractEventDrivenEntity<ConnectionEvent, Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The status of the last completed sync attempt, which indicates whether the connection was successfully synced,
     * or if additional steps need to be completed.
     */
    @Enumerated(value = EnumType.STRING)
    private ConnectionStatus status = ConnectionStatus.PENDING;

    /**
     * If a connection is disabled, it cannot be synced,
     * and its associated accounts cannot have data retrieved for them.
     */
    private Boolean isDisabled;

    /**
     * Individual financial accounts held under a {@code Connection} to a financial institution.
     * A connection may contain multiple accounts.
     * <p/>
     * For example, a single bank connection might include a savings account,
     * a checking account, and a mortgage.
     */
    @JsonIgnore
    @ElementCollection
    @CollectionTable(name = "accounts")
    private List<Long> accountIds;

    @JsonProperty("connectionId")
    @Override
    public Long getIdentity() {
        return id;
    }

    public void setIdentity(Long id) {
        this.id = id;
    }

    /**
     * Returns the {@link Link} with a rel of {@link Link#REL_SELF}.
     */
    @Override
    public Link getId() {
        return linkTo(AuthDepositController.class)
                .slash("connections")
                .slash(getIdentity())
                .withSelfRel();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends AggregateServiceProvider<A>, A extends Aggregate<ConnectionEvent, Long>> T getServiceProvider() throws
            IllegalArgumentException {
        ConnectionServiceProvider connectionModule = getServiceProvider(ConnectionServiceProvider.class);
        return (T) connectionModule;
    }

    public ConnectionStatus getStatus() {
        return status;
    }

    public void setStatus(ConnectionStatus status) {
        this.status = status;
    }

    public Boolean getDisabled() {
        return isDisabled;
    }

    public void setDisabled(Boolean disabled) {
        isDisabled = disabled;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("status", status)
                .add("isDisabled", isDisabled)
                .toString();
    }
}
