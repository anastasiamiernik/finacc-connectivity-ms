package demo.connection.domain;

import org.springframework.stereotype.Service;

import demo.authdeposit.event.AuthDepositEvent;
import demo.domain.eventdriven.AggregateServiceProvider;
import demo.domain.eventdriven.event.EventService;

@Service
public class ConnectionServiceProvider extends AggregateServiceProvider<Connection> {

    private final ConnectionService connectionService;
    private final EventService<AuthDepositEvent, Long> eventService;

    public ConnectionServiceProvider(ConnectionService connectionService, EventService<AuthDepositEvent, Long> eventService) {
        this.connectionService = connectionService;
        this.eventService = eventService;
    }

    public ConnectionService getConnectionService() {
        return connectionService;
    }

    public EventService<AuthDepositEvent, Long> getEventService() {
        return eventService;
    }

    @Override
    public ConnectionService getDefaultService() {
        return connectionService;
    }

    @Override
    public EventService<AuthDepositEvent, Long> getDefaultEventService() {
        return eventService;
    }
}
