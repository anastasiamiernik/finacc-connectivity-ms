package demo.config.aws;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.function.lambda.AuthDepositLambdaService;
import demo.util.MapperAssist;

@Configuration
public class AwsLambdaConfig {

    private final BasicAWSCredentials awsCredentials = new BasicAWSCredentials("ACESS_KEY", "SECREY_KEY");

    @Bean
    public AwsLambdaInvoker lambdaInvoker() {
        AuthDepositLambdaService lambdaService = LambdaInvokerFactory.builder()
                .lambdaClient(getLambdaClient())
                .build(AuthDepositLambdaService.class);

        return new AwsLambdaInvoker(lambdaService);
    }

    @Bean
    public MapperAssist mapperAssist(ObjectMapper mapper) {
        return new MapperAssist(mapper);
    }

    public static class AwsLambdaInvoker {

        private final AuthDepositLambdaService lambdaService;

        AwsLambdaInvoker(AuthDepositLambdaService lambdaService) {
            this.lambdaService = lambdaService;
        }

        public AuthDepositLambdaService getLambdaService() {
            return lambdaService;
        }
    }

    // Sample demo lambda client, with static CredentialsProvider
    public AWSLambda getLambdaClient() {
        return AWSLambdaClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }
}
