package demo.connection.event;

/**
 * Todo.
 */
public enum ConnectionEventType {
    CONNECTION_INITIATED,
    CONNECTION_CREATED,
    CONNECTION_FAILED
}
