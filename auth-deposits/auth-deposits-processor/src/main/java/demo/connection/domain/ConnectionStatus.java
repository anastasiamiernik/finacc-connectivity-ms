package demo.connection.domain;

/**
 * The status of the last completed sync attempt, which indicates whether the connection was successfully synced,
 * or if additional steps need to be completed.
 */
public enum ConnectionStatus {

    /**
     * Connection has been initiated but not synced.
     */
    PENDING,

    /**
     * The connection has been properly synced.
     */
    OK,

    /**
     * The login credentials for the connection are incorrect.
     */
    INCORRECT_CREDENTIALS,

    /**
     * The institution requires the end user to log into the connection and resolve an issue.
     */
    USER_CONFIG,

    /**
     * The connection needs to be resynced to complete the sync process.
     */
    RESYNC,

    /**
     * The institution is inaccessible at the moment.
     */
    POSTPONED,

    /**
     * Issues while syncing the connection.
     */
    MAINTENANCE,

    /**
     * No accounts found within the connection.
     */
    NO_ACCOUNTS,

    /**
     * Temporarily unable to sync any connections at this institution.
     */
    INSTITUTION_UNAVAILABLE
}
