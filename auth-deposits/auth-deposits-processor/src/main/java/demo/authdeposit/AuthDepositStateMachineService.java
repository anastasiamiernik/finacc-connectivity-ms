package demo.authdeposit;

import java.net.URI;
import java.util.Comparator;
import java.util.UUID;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import demo.authdeposit.event.AuthDepositVerificationEvents;
import demo.domain.HATEOASEntity;
import lombok.NonNull;

/**
 * The {@link AuthDepositStateMachineService} provides methods to create and start a new state machine
 * and to replicate the state of an {@link AuthDeposit} aggregate entity.
 */
@Service
public class AuthDepositStateMachineService {

    private final StateMachineFactory<AuthDepositVerificationState, AuthDepositVerificationEventType> factory;
    private final RestTemplate restTemplate;

    public AuthDepositStateMachineService(StateMachineFactory<AuthDepositVerificationState, AuthDepositVerificationEventType>
            factory,
            RestTemplate restTemplate) {
        this.factory = factory;
        this.restTemplate = restTemplate;
    }

    /**
     * Creates and starts a new state machine.
     */
    public StateMachine<AuthDepositVerificationState, AuthDepositVerificationEventType> newStateMachine() {
        StateMachine<AuthDepositVerificationState, AuthDepositVerificationEventType> stateMachine =
                factory.getStateMachine(UUID.randomUUID().toString());
        stateMachine.start();
        return stateMachine;
    }

    /**
     * Replicates the state of an {@link AuthDeposit} from a sequence of {@link AuthDepositVerificationEvent}s.
     *
     * @param stateMachine state machine for replicating an aggregate's state
     * @param event received event
     *
     * @return state machine instance after replication
     */
    public StateMachine<AuthDepositVerificationState, AuthDepositVerificationEventType> replicateState(
            @NonNull StateMachine<AuthDepositVerificationState, AuthDepositVerificationEventType> stateMachine,
            @NonNull AuthDepositVerificationEvent event) {
        Link eventId = event.getId();
        AuthDepositVerificationEvents eventLog = getEventLog(event);
        // For each event in the log trigger events
        eventLog.getContent().stream()
                .sorted(Comparator.comparing(HATEOASEntity::getCreatedAt))
                .forEach(e -> stateMachine.sendEvent(e.getId().equals(eventId) ?
                        getMessageWithHeaders(e) :
                        getMessageWithPayload(e)));

        return stateMachine;
    }

    public StateMachine<AuthDepositVerificationState, AuthDepositVerificationEventType> replicateState(
            @NonNull AuthDepositVerificationEvent event) {
        StateMachine<AuthDepositVerificationState, AuthDepositVerificationEventType> stateMachine = newStateMachine();
        Link eventId = event.getId();
        AuthDepositVerificationEvents eventLog = getEventLog(event);
        // For each event in the log trigger events
        eventLog.getContent().stream()
                .sorted(Comparator.comparing(HATEOASEntity::getCreatedAt))
                .forEach(e -> stateMachine.sendEvent(e.getId().equals(eventId) ?
                        getMessageWithHeaders(e) :
                        getMessageWithPayload(e)));

        return stateMachine;
    }

    private Message<AuthDepositVerificationEventType> getMessageWithPayload(@NonNull AuthDepositVerificationEvent event) {
        return MessageBuilder
                .withPayload(event.getType())
                .build();
    }

    private Message<AuthDepositVerificationEventType> getMessageWithHeaders(@NonNull AuthDepositVerificationEvent event) {
        return MessageBuilder
                .withPayload(event.getType())
                .setHeader("event", event)
                .build();
    }

    private AuthDepositVerificationEvents getEventLog(AuthDepositVerificationEvent event) {
        URI href = URI.create(event.getLink("deposit").getHref());
        Traverson traverson = new Traverson(href, MediaTypes.HAL_JSON);
        traverson.setRestOperations(restTemplate);
        return traverson.follow("events")
                .toEntity(AuthDepositVerificationEvents.class)
                .getBody();
    }
}
