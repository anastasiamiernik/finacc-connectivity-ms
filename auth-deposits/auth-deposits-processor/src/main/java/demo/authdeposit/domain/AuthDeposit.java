package demo.authdeposit.domain;

import java.util.HashSet;
import java.util.Set;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonProperty;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.domain.HATEOASEntity;

public class AuthDeposit extends HATEOASEntity {

    private Long id;
    private Set<AuthDepositVerificationEvent> events = new HashSet<>();
    private AuthDepositVerificationState state;
    private Long accountId;

    /**
     * Returns the {@link Link} with a rel of {@link Link#REL_SELF}.
     */
    @Override
    public Link getId() {
        return getLink("self");
    }

    @JsonProperty("authDepositId")
    public Long getIdentity() {
        return this.id;
    }

    public void setIdentity(Long id) {
        this.id = id;
    }

    public Set<AuthDepositVerificationEvent> getEvents() {
        return events;
    }

    public void setEvents(Set<AuthDepositVerificationEvent> events) {
        this.events = events;
    }

    public AuthDepositVerificationState getState() {
        return state;
    }

    public void setState(AuthDepositVerificationState state) {
        this.state = state;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }
}
