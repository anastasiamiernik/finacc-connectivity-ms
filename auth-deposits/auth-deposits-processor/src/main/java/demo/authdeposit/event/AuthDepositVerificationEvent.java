package demo.authdeposit.event;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;
import demo.domain.HATEOASEntity;

public class AuthDepositVerificationEvent extends HATEOASEntity {

    private AuthDepositVerificationEventType type;

    public AuthDepositVerificationEvent() {
    }

    public AuthDepositVerificationEvent(AuthDepositVerificationEventType type) {
        this.type = type;
    }

    public AuthDepositVerificationEventType getType() {
        return type;
    }

    public void setType(AuthDepositVerificationEventType type) {
        this.type = type;
    }

    @JsonIgnore
    @Override
    public Link getId() {
        return super.getId();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .toString();
    }
}
