package demo.authdeposit;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.hateoas.Link;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class AuthDepositStateManager {

    private static final String AUTH_DEPOSIT_STRING = "deposit";
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthDepositStateManager.class);

    private final AuthDepositStateMachineService stateMachineService;
    private final DiscoveryClient discoveryClient;

    public AuthDepositStateManager(AuthDepositStateMachineService stateMachineService, DiscoveryClient discoveryClient) {
        this.stateMachineService = stateMachineService;
        this.discoveryClient = discoveryClient;
    }

    /**
     * Applies event {@code event} and returns the resulting aggregate entity.
     *
     * @param event received event
     */
    public AuthDeposit applyEvent(final AuthDepositVerificationEvent event) {
        checkNotNull(event, "Cannot apply a null event");
        checkNotNull(event.getId(), "The event identity link can't be null");
        LOGGER.info("Auth deposit verification event received: ID {}, Type {}", event.getId(), event.getType());

        List<ServiceInstance> serviceInstances = discoveryClient.getInstances("auth-deposits-api");
        ServiceInstance authDepositService = serviceInstances.get(new Random().nextInt(serviceInstances.size()));
        URI depositHref = getUri(authDepositService, URI.create(event.getLink(AUTH_DEPOSIT_STRING).getHref()));
        URI selfHref = getUri(authDepositService, URI.create(event.getLink("self").getHref()));
        event.getLinks()
                .replaceAll(a -> Objects.equals(a.getRel(), AUTH_DEPOSIT_STRING) ?
                        new Link(depositHref.toString(), AUTH_DEPOSIT_STRING) : a);
        event.getLinks()
                .replaceAll(a -> Objects.equals(a.getRel(), "self") ?
                        new Link(selfHref.toString(), "self") : a);

        StateMachine<AuthDepositVerificationState, AuthDepositVerificationEventType> stateMachine =
                stateMachineService.replicateState(event);
        stateMachine.stop();

        return stateMachine.getExtendedState().get(AUTH_DEPOSIT_STRING, AuthDeposit.class);
    }

    private URI getUri(ServiceInstance serviceInstance, URI uri) {
        return URI.create(uri.toString()
                .replace(uri.getHost(), serviceInstance.getHost())
                .replace(":" + uri.getPort(), ":" + String.valueOf(serviceInstance.getPort())));
    }
}
