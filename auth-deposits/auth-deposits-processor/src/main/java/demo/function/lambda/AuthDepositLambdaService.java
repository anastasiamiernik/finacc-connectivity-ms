package demo.function.lambda;

import com.amazonaws.services.lambda.invoke.LambdaFunction;
import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.event.AuthDepositVerificationEvent;

public interface AuthDepositLambdaService {

    @LambdaFunction(functionName = "send-microdeposits")
    AuthDeposit sendMicrodeposits(AuthDepositVerificationEvent event);
}
