package demo.function.lambda;

import org.springframework.statemachine.StateContext;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import demo.function.BaseAuthDepositFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An example of a {@link BaseAuthDepositFunction} implementation that invokes AWS Lambda Function via proxy object.
 */
public class AuthDepositInitiated extends BaseAuthDepositFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthDepositInitiated.class);

    private final AuthDepositLambdaService lambdaService;

    public AuthDepositInitiated(StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context,
            AuthDepositLambdaService lambdaService) {
        super(context, null);
        this.lambdaService = lambdaService;
    }

    /**
     * Applies the {@link AuthDepositVerificationEvent} to the {@link AuthDeposit} aggregate.
     *
     * @param event is the {@link AuthDepositVerificationEvent} for this context
     */
    @Override
    public AuthDeposit apply(AuthDepositVerificationEvent event) {
        LOGGER.info("Executing workflow for initiated auth deposit...");
        return applyLambda(event).getPayload();
    }

    public LambdaResponse<AuthDeposit> applyLambda(AuthDepositVerificationEvent event) {
        try {
            return new LambdaResponse<>(lambdaService.sendMicrodeposits(event));
        } catch (Exception exception) {
            LOGGER.error("Error applying lambda function on event {}", event.getType());
            return new LambdaResponse<>(exception, null);
        }
    }
}
