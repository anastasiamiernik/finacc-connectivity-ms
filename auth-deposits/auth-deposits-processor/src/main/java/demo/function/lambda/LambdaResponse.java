package demo.function.lambda;

import com.google.common.base.MoreObjects;

public class LambdaResponse<T> {

    private Exception exception;
    private T payload;

    public LambdaResponse(Exception exception, T payload) {
        this.exception = exception;
        this.payload = payload;
    }

    public LambdaResponse(Exception exception) {
        this.exception = exception;
    }

    public LambdaResponse(T payload) {
        this.payload = payload;
    }

    public Exception getException() {
        return exception;
    }

    public T getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("exception", exception)
                .add("payload", payload)
                .toString();
    }
}
