package demo.function.lambda;

import org.springframework.cloud.function.adapter.aws.SpringBootRequestHandler;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.event.AuthDepositVerificationEvent;

public class SendMicrodepositsFunctionHandler extends SpringBootRequestHandler<AuthDepositVerificationEvent, AuthDeposit> {

}
