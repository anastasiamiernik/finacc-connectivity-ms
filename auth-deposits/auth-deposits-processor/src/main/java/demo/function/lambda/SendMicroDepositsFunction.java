package demo.function.lambda;

import java.net.URI;
import java.util.function.Function;

import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A function implementation to be invoked via {@link AuthDepositLambdaService}.
 */
public class SendMicroDepositsFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendMicroDepositsFunction.class);

    @Bean
    public Function<AuthDepositVerificationEvent, AuthDeposit> function() {
        return event -> {
            LOGGER.info(event.getType() + ": " + getEntityHref(event));
            Traverson traverson = getResourceForEvent(event);
            return traverson.follow("self", "commands", "sendMicrodeposits")
                    .toObject(AuthDeposit.class);
        };
    }

    private Traverson getResourceForEvent(AuthDepositVerificationEvent event) {
        return new Traverson(
                URI.create(getEntityHref(event)),
                MediaTypes.HAL_JSON
        );
    }

    private String getEntityHref(AuthDepositVerificationEvent event) {
        return event.getLink("deposit").getHref();
    }

}
