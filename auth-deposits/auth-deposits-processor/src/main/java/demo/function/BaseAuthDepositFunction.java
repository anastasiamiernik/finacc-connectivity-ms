package demo.function;

import java.util.function.Function;

import org.springframework.statemachine.StateContext;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link BaseAuthDepositFunction} maps {@link org.springframework.statemachine.action.Action}s
 * triggered by state transitions in the course of auth deposit verification workflow
 * to a function.
 */
public abstract class BaseAuthDepositFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseAuthDepositFunction.class);

    protected final StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context;
    protected final Function<AuthDepositVerificationEvent, AuthDeposit> authDepositFunction;

    /**
     * Create a new instance of {@link BaseAuthDepositFunction} by supplying a state context and a function,
     * the latter to be used to apply {@link AuthDepositVerificationEvent} to a provided action.
     *
     * @param context is the {@link StateContext} for a replicated state machine
     * @param authDepositFunction is the lambda function describing an action that consumes an
     * {@link AuthDepositVerificationEvent}
     */
    public BaseAuthDepositFunction(
            StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context,
            Function<AuthDepositVerificationEvent, AuthDeposit> authDepositFunction) {
        this.context = context;
        this.authDepositFunction = authDepositFunction;
    }

    /**
     * Apply an {@link AuthDepositVerificationEvent} to the function that was provided through the
     * constructor of this {@link BaseAuthDepositFunction}.
     *
     * @param event is the {@link AuthDepositVerificationEvent} to apply to the function
     */
    public AuthDeposit apply(AuthDepositVerificationEvent event) {
        AuthDeposit result = authDepositFunction.apply(event);
        context.getExtendedState().getVariables().put("deposit", result);
        LOGGER.info("AuthDeposit function: " + event.getType());
        return result;
    }
}
