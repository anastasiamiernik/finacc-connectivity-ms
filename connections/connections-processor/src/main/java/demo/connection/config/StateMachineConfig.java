package demo.connection.config;

import java.net.URI;
import java.util.EnumSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionState;
import demo.connection.event.ConnectionEvent;
import demo.connection.event.ConnectionEventType;
import demo.connection.function.BaseConnectionFunction;
import demo.connection.function.ConnectionCreated;
import demo.connection.function.ConnectionFailed;
import demo.connection.function.SyncStarted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Defines a state machine to manage connection synchronization workflow.
 * <p>
 * A state machine provides a declarative language for describing the state of an {@link demo.connection.domain.Connection}
 * resource given a sequence of ordered {@link demo.connection.event.ConnectionEvents}.
 */
@Configuration
@EnableStateMachineFactory
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<ConnectionState, ConnectionEventType> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateMachineConfig.class);

    /**
     * Configures the initial conditions of a new in-memory {@link StateMachine} for Auth Deposit Verification workflow.
     *
     * @param states is the {@link StateMachineStateConfigurer} used to describe the initial condition
     */
    @Override
    public void configure(StateMachineStateConfigurer<ConnectionState, ConnectionEventType> states) {
        try {
            states.withStates()
                    .initial(ConnectionState.CONNECTION_CREATED)
                    .states(EnumSet.allOf(ConnectionState.class));
        } catch (Exception e) {
            throw new RuntimeException("State machine configuration failed", e);
        }
    }

    /**
     * The {@link BaseConnectionFunction} argument is only applied
     * if an {@link ConnectionEvent} is provided as a message header in the {@link StateContext}.
     *
     * @param context is the state machine context that may include an {@link ConnectionEvent}
     * @param connectionFunction is the function to apply after the state machine has completed replication
     *
     * @return an {@link ConnectionEvent} only if this event has not yet been processed,
     * otherwise returns null
     */
    private ConnectionEvent applyEvent(
            StateContext<ConnectionState, ConnectionEventType> context,
            BaseConnectionFunction connectionFunction) {
        ConnectionEvent event = null;
        if (context.getMessageHeader("event") != null) {
            event = context.getMessageHeaders().get("event", ConnectionEvent.class);
            LOGGER.info("State replication complete for event: {}", event);
            connectionFunction.apply(event);
        }

        return event;
    }

    /**
     * Configures the {@link StateMachine} that describes how {@link ConnectionEventType} drives the state
     * of an {@link demo.connection.domain.Connection}.
     * <p>
     * Events are triggered on transitions from a source {@link ConnectionEvent}
     * to a target {@link ConnectionState}.
     * An {@link Action} is attached to each transition, which maps to a function that is executed
     * in the context of an {@link ConnectionEvent}.
     *
     * @param transitions is the {@link StateMachineTransitionConfigurer} used to describe state transitions
     */
    @Override
    public void configure(StateMachineTransitionConfigurer<ConnectionState, ConnectionEventType> transitions) {
        try {
            transitions.withExternal()
                    .source(ConnectionState.CONNECTION_INITIATED)
                    .target(ConnectionState.CONNECTION_CREATED)
                    .event(ConnectionEventType.CONNECTION_CREATED)
                    .action(connectionCreated())
                    .and()
                    .withExternal()
                    .source(ConnectionState.CONNECTION_INITIATED)
                    .target(ConnectionState.CONNECTION_FAILED)
                    .event(ConnectionEventType.CONNECTION_FAILED)
                    .action(connectionFailed())
                    .and()
                    .withExternal()
                    .source(ConnectionState.CONNECTION_CREATED)
                    .target(ConnectionState.SYNC_STARTED)
                    .event(ConnectionEventType.SYNC_STARTED)
                    .action(syncStarted());
        } catch (Exception e) {
            throw new RuntimeException("Could not configure state machine transitions", e);
        }
    }

    @Bean
    public Action<ConnectionState, ConnectionEventType> connectionCreated() {
        return context -> applyEvent(context, new ConnectionCreated(context));
    }

    @Bean
    public Action<ConnectionState, ConnectionEventType> connectionFailed() {
        return context -> applyEvent(context, new ConnectionFailed(context));
    }

    @Bean
    public Action<ConnectionState, ConnectionEventType> syncStarted() {
        return context -> applyEvent(context,
                new SyncStarted(context, event -> {
                    LOGGER.info(event.getType() + ": " + event.getLink("connection").getHref());
                    // Get the connection resource for the event
                    Traverson traverson = new Traverson(
                            URI.create(event.getLink("connection").getHref()),
                            MediaTypes.HAL_JSON
                    );
                    return traverson.follow("self", "commands", "sync")
                            .toObject(Connection.class);
                }));
    }
}
