package demo.connection.function;

import java.util.function.Function;

import org.springframework.statemachine.StateContext;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionState;
import demo.connection.event.ConnectionEvent;
import demo.connection.event.ConnectionEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SyncStarted extends BaseConnectionFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(SyncStarted.class);

    public SyncStarted(StateContext<ConnectionState, ConnectionEventType> context) {
        this(context, null);
    }

    public SyncStarted(StateContext<ConnectionState, ConnectionEventType> context,
            Function<ConnectionEvent, Connection> function) {
        super(context, function);
    }

    /**
     * Applies the {@link ConnectionEvent} to the {@link Connection} aggregate.
     *
     * @param event is the {@link ConnectionEvent} for this context
     */
    @Override
    public Connection apply(ConnectionEvent event) {
        LOGGER.info("Executing workflow for started sync...");
        return super.apply(event);
    }
}
