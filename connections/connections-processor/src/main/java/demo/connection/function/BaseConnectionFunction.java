package demo.connection.function;

import java.util.function.Function;

import org.springframework.statemachine.StateContext;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionState;
import demo.connection.event.ConnectionEvent;
import demo.connection.event.ConnectionEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link BaseConnectionFunction} maps {@link org.springframework.statemachine.action.Action}s
 * triggered by state transitions in the course of connection synchronization workflow
 * to a function.
 */
public abstract class BaseConnectionFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseConnectionFunction.class);

    protected final StateContext<ConnectionState, ConnectionEventType> context;
    protected final Function<ConnectionEvent, Connection> connectionFunction;

    /**
     * Create a new instance of {@link BaseConnectionFunction} by supplying a state context and a function,
     * the latter to be used to apply {@link ConnectionEvent} to a provided action.
     *
     * @param context is the {@link StateContext} for a replicated state machine
     * @param connectionFunction is the lambda function describing an action that consumes an
     * {@link ConnectionEvent}
     */
    public BaseConnectionFunction(StateContext<ConnectionState, ConnectionEventType> context,
            Function<ConnectionEvent, Connection> connectionFunction) {
        this.context = context;
        this.connectionFunction = connectionFunction;
    }

    /**
     * Apply an {@link ConnectionEvent} to the function that was provided through the
     * constructor of this {@link BaseConnectionFunction}.
     *
     * @param event is the {@link ConnectionEvent} to apply to the function
     */
    public Connection apply(ConnectionEvent event) {
        Connection result = connectionFunction.apply(event);
        context.getExtendedState().getVariables().put("connection", result);
        LOGGER.info("Connection function: " + event.getType());
        return result;
    }
}
