package demo.connection.event;

import com.google.common.base.MoreObjects;
import demo.domain.HATEOASEntity;

public class ConnectionEvent extends HATEOASEntity {

    private ConnectionEventType type;

    public ConnectionEvent() {
    }

    public ConnectionEvent(ConnectionEventType type) {
        this.type = type;
    }

    public ConnectionEventType getType() {
        return type;
    }

    public void setType(ConnectionEventType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .toString();
    }
}
