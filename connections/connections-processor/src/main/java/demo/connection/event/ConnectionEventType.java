package demo.connection.event;

public enum ConnectionEventType {
    CONNECTION_INITIATED,
    CONNECTION_CREATED,
    CONNECTION_FAILED,
    SYNC_STARTED
}
