package demo.connection;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.hateoas.Link;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionState;
import demo.connection.event.ConnectionEvent;
import demo.connection.event.ConnectionEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class ConnectionStateManager {

    private static final String CONNECTION_STRING = "connection";
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionStateManager.class);

    private final ConnectionStateMachineService stateMachineService;
    private final DiscoveryClient discoveryClient;

    public ConnectionStateManager(ConnectionStateMachineService stateMachineService, DiscoveryClient discoveryClient) {
        this.stateMachineService = stateMachineService;
        this.discoveryClient = discoveryClient;
    }

    /**
     * Applies event {@code event} and returns the resulting aggregate entity.
     *
     * @param event received event
     */
    public Connection applyEvent(final ConnectionEvent event) {
        checkNotNull(event, "Cannot apply a null event");
        checkNotNull(event.getId(), "The event identity link can't be null");
        LOGGER.info("Auth deposit verification event received: ID {}, Type {}", event.getId(), event.getType());

        List<ServiceInstance> serviceInstances = discoveryClient.getInstances("connections-api");
        ServiceInstance connectionService = serviceInstances.get(new Random().nextInt(serviceInstances.size()));
        URI depositHref = getUri(connectionService, URI.create(event.getLink(CONNECTION_STRING).getHref()));
        URI selfHref = getUri(connectionService, URI.create(event.getLink("self").getHref()));
        event.getLinks()
                .replaceAll(a -> Objects.equals(a.getRel(), CONNECTION_STRING) ?
                        new Link(depositHref.toString(), CONNECTION_STRING) : a);
        event.getLinks()
                .replaceAll(a -> Objects.equals(a.getRel(), "self") ?
                        new Link(selfHref.toString(), "self") : a);

        StateMachine<ConnectionState, ConnectionEventType> stateMachine =
                stateMachineService.replicateState(event);
        stateMachine.stop();

        return stateMachine.getExtendedState().get(CONNECTION_STRING, Connection.class);
    }

    private URI getUri(ServiceInstance serviceInstance, URI uri) {
        return URI.create(uri.toString()
                .replace(uri.getHost(), serviceInstance.getHost())
                .replace(":" + uri.getPort(), ":" + String.valueOf(serviceInstance.getPort())));
    }
}
