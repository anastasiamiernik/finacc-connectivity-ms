package demo.domain.eventdriven;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import demo.domain.eventdriven.event.EventService;

/**
 * A {@link AggregateServiceProvider} provides services to operate on an {@link Aggregate} and its events.
 */
@Component
public abstract class AggregateServiceProvider<T extends Aggregate> implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        AggregateServiceProvider.applicationContext = applicationContext;
    }

    public abstract CrudService<? extends Aggregate, ?> getDefaultService();

    public abstract EventService<?, ?> getDefaultEventService();
}
