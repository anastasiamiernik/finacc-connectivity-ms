package demo.account.domain;

public enum AccountCategory {
    INVESMENT,
    INSURANCE,
    BANKING,
    LOAN,
    OTHER
}
