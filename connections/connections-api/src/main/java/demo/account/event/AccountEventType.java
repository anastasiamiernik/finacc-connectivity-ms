package demo.account.event;

public enum AccountEventType {
    ACCOUNT_CREATED,
    ACCOUNT_CONNECTED
}
