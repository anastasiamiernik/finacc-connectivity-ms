package demo.connection.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import demo.account.domain.Account;
import demo.connection.api.v1.ConnectionController;
import demo.connection.command.CreateConnection;
import demo.connection.command.UpdateConnectionState;
import demo.connection.event.ConnectionEvent;
import demo.domain.eventdriven.AbstractEventDrivenEntity;
import demo.domain.eventdriven.Aggregate;
import demo.domain.eventdriven.AggregateServiceProvider;
import lombok.Builder;
import lombok.NonNull;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * A connection is a representation of the link between end user and a specific financial institution.
 * It’s through this connection that an interface with an institution is established
 * and the user’s account data is synced.
 */
@Builder
@Entity
public class Connection extends AbstractEventDrivenEntity<ConnectionEvent, Long> {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * The status of the last completed sync attempt, which indicates whether the connection was successfully synced,
     * or if additional steps need to be completed.
     */
    @Enumerated(value = EnumType.STRING)
    private ConnectionSyncState status;

    /**
     * Represents state transitions in the course of connection sync workflow.
     * <p/>
     * The aggregate state of a {@link Connection} is sourced from {@link ConnectionEvent}.
     */
    @Enumerated(value = EnumType.STRING)
    private ConnectionSyncState state;

    /**
     * If a connection is disabled, it cannot be synced,
     * and its associated accounts cannot have data retrieved for them.
     */
    private Boolean isDisabled;

    /**
     * Individual financial account held under a {@code Connection} to a financial institution.
     */
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Account account;

    // TODO: add last good sync attempt time
    // TODO: add last successful sync time

    @JsonProperty("connectionId")
    @Override
    public Long getIdentity() {
        return id;
    }

    public void setIdentity(Long id) {
        this.id = id;
    }

    public ConnectionSyncState getState() {
        return state;
    }

    public void setState(ConnectionSyncState state) {
        this.state = state;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Returns the {@link Link} with a rel of {@link Link#REL_SELF}.
     */
    @Override
    public Link getId() {
        return linkTo(ConnectionController.class)
                .slash("connections")
                .slash(getIdentity())
                .withSelfRel();
    }

    // Methods to modify Connection state through commands

    public Connection createConnection(Long accountId) {
        return getCommand(CreateConnection.class)
                .execute(accountId);
    }

    public Connection updateConnectionState(ConnectionSyncState connectionState) {
        return getCommand(UpdateConnectionState.class)
                .execute(this, connectionState);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends AggregateServiceProvider<A>, A extends Aggregate<ConnectionEvent, Long>> T getServiceProvider() throws
            IllegalArgumentException {
        ConnectionServiceProvider connectionModule = getServiceProvider(ConnectionServiceProvider.class);
        return (T) connectionModule;
    }

    public Boolean getDisabled() {
        return isDisabled;
    }

    public void setDisabled(Boolean disabled) {
        isDisabled = disabled;
    }

    public Connection setFrom(@NonNull Connection other) {
        this.setAccount(other.account);
        this.setDisabled(other.isDisabled);
        this.setState(other.getState());
        this.setState(other.getState());
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("status", status)
                .add("state", state)
                .add("isDisabled", isDisabled)
                .add("account", account)
                .toString();
    }
}
