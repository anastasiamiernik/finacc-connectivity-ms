package demo.connection.domain;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;

public class Connections extends Resources<Connection> {

    /**
     * Creates a {@link Resources} instance with the given content and {@link Link}s (optional).
     *
     * @param content must not be {@literal null}.
     * @param links the links to be added to the {@link Resources}.
     */
    public Connections(Iterable<Connection> content, Link... links) {
        super(content, links);
    }
}
