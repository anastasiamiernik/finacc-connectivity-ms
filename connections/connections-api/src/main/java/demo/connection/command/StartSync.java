package demo.connection.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionService;
import demo.connection.domain.ConnectionSyncState;
import demo.connection.event.ConnectionEvent;
import demo.connection.event.ConnectionSyncEventType;
import demo.domain.eventdriven.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkState;

@Service
@Transactional
public class StartSync extends Command<Connection> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartSync.class);
    private final ConnectionService connectionService;

    public StartSync(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public Connection execute(Connection connection) {
        // Make assumptions on the aggregate's state
        checkState(connection.getState() == ConnectionSyncState.CONNECTION_CREATED,
                "Connection State should be CONNECTION CREATED");

        // FIXME: add impl via external services

        try {
            connection.sendAsyncEvent(new ConnectionEvent(ConnectionSyncEventType.SYNC_STARTED, connection));
        } catch (Exception exception) {
            LOGGER.error("Failed to initiate sync process", exception);
            // Rollback to previous state
            connection.setState(ConnectionSyncState.CONNECTION_CREATED);
            connection = connectionService.update(connection);
        }

        return connection;
    }
}
