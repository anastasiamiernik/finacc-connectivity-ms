package demo.connection.api.v1;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.annotation.Nullable;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionService;
import demo.connection.domain.ConnectionSyncState;
import demo.connection.event.ConnectionEvent;
import demo.connection.exception.ConnectionNotFoundException;
import demo.domain.eventdriven.event.EventService;
import demo.domain.eventdriven.event.Events;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/v1")
public class ConnectionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionController.class);

    private final ConnectionService connectionService;
    private final EventService<ConnectionEvent, Long> eventService;
    private final ConnectionResourceAssembler resourceAssembler;

    public ConnectionController(ConnectionService connectionService, EventService<ConnectionEvent, Long> eventService,
            ConnectionResourceAssembler resourceAssembler) {
        this.connectionService = connectionService;
        this.eventService = eventService;
        this.resourceAssembler = resourceAssembler;
    }

    @PostMapping(path = "/connections")
    public ResponseEntity createConnection(@RequestBody Connection connection) {
        return Optional.ofNullable(createConnectionResource(connection))
                .map(resource -> ResponseEntity
                        .created(getURI(resource))
                        .body(resource))
                .orElseThrow(() -> new RuntimeException("Connection creation failed"));
    }

    @PutMapping(path = "/connections/{id}")
    public ResponseEntity updateConnection(@RequestBody Connection connection, @PathVariable Long id) {
        return Optional.ofNullable(updateConnectionResource(connection, id))
                .map(resource -> ResponseEntity
                        .created(getURI(resource))
                        .body(resource))
                .orElseThrow(() -> new RuntimeException("Connection update failed"));
    }

    @RequestMapping(path = "/connections/{id}")
    public Resource<Connection> getConnection(@PathVariable Long id) {
        return connectionService.get(id)
                .map(resourceAssembler::toResource)
                .orElseThrow(ConnectionNotFoundException::new);
    }

    @DeleteMapping(path = "/connections/{id}")
    public ResponseEntity deleteConnection(@PathVariable Long id) {
        connectionService.delete(id);
        return ResponseEntity
                .noContent()
                .build();
    }

    @RequestMapping(path = "/connections/{id}/events")
    public ResponseEntity getConnectionEvents(@PathVariable Long id) {
        return Optional.of(getEventResources(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new RuntimeException("Could not get events for Connection " + id));
    }

    @RequestMapping(path = "/connections/{id}/events/{eventId}")
    public ResponseEntity getConnectionEvent(@PathVariable Long id, @PathVariable Long eventId) {
        return Optional.of(getEventResource(eventId))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new RuntimeException("Could not get connection event " + eventId));
    }

    @PostMapping(path = "/connections/{id}/events")
    public ResponseEntity createConnectionEvent(@PathVariable Long id, @RequestBody ConnectionEvent event) {
        Resource<ConnectionEvent> eventResource = appendEventResource(id, event);
        return ResponseEntity
                .created(getURI(eventResource))
                .body(eventResource);
    }

    @RequestMapping(path = "/connections/{id}/commands")
    public ResponseEntity getConnectionCommands(@PathVariable Long id) {
        return Optional.of(getCommandsResources(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new RuntimeException("Could not get commands for connection " + id));
    }

    @RequestMapping(path = "/connections/{id}/commands/updateConnectionState")
    public ResponseEntity updateConnectionState(@PathVariable Long id, @RequestParam(value = "state") ConnectionSyncState
            state) {
        return connectionService.get(id)
                .map(connection -> connection.updateConnectionState(state))
                .map(resourceAssembler::toResource)
                .map(resource -> new ResponseEntity<>(resource, HttpStatus.OK))
                .orElseThrow(ConnectionNotFoundException::new);
    }

    @RequestMapping(path = "/connections/search/findConnectionByAccountId")
    public Resource<Connection> findConnectionByAccountId(@RequestParam("accountId") Long accountId) {
        return connectionService.getConnectionByAccountId(accountId)
                .map(resourceAssembler::toResource)
                .orElseThrow(ConnectionNotFoundException::new);
    }

    @Nullable
    private Resource<Connection> createConnectionResource(@NonNull Connection connection) {
        return resourceAssembler.toResource(connectionService.registerWithEventFlow(connection));
    }

    @Nullable
    private Resource<Connection> updateConnectionResource(@NonNull Connection connection, Long id) {
        Connection connectionToSave = connectionService.get(id)
                .map(c -> c.setFrom(connection))
                .orElseGet(() -> {
                    connection.setIdentity(id);
                    return connection;
                });
        return resourceAssembler.toResource(connectionService.update(connectionToSave));
    }

    private Resource<ConnectionEvent> appendEventResource(Long connectionId, ConnectionEvent event) {
        checkNotNull(event, "Event body must be provided");
        Connection connection = connectionService.get(connectionId)
                .orElseThrow(ConnectionNotFoundException::new);
        event.setEntity(connection);
        // Send ConnectionEvent
        connection.sendAsyncEvent(event);
        LOGGER.warn("Appending connection events");
        return new Resource<>(event,
                linkTo(ConnectionController.class)
                        .slash("connections")
                        .slash(connectionId)
                        .slash("events")
                        .slash(event.getEventId())
                        .withSelfRel(),
                linkTo(ConnectionController.class)
                        .slash("connections")
                        .slash(connectionId)
                        .withRel("connection")
        );
    }

    private Optional<ConnectionEvent> getEventResource(Long eventId) {
        return eventService.findOne(eventId);
    }

    private Events getEventResources(Long id) {
        return eventService.find(id);
    }

    private ResourceSupport getCommandsResources(Long id) {
        Connection connection = Connection.builder().id(id).build();
        return new Resource<>(connection.getCommands());
    }

    private URI getURI(Resource<? extends ResourceSupport> resource) {
        URI uri = null;
        try {
            uri = new URI(resource.getId().expand().getHref());
        } catch (URISyntaxException e) {
            LOGGER.error("Wrong URI syntax", new IllegalStateException("Error creating URI for resource" +
                    resource.toString()));
        }
        return uri;
    }
}
