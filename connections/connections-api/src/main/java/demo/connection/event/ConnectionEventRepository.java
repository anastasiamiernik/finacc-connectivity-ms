package demo.connection.event;

import demo.domain.eventdriven.event.EventRepository;

public interface ConnectionEventRepository extends EventRepository<ConnectionEvent, Long> {

}
